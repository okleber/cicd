from unittest import TestCase
from square import square


class TestSquare(TestCase):
    def test_square_int(self):
        self.assertEqual(9,square(3),msg="Expected 9 int")
    def test_square_string(self):
        self.assertEqual(None,square("a"),msg="Expected None for string")
