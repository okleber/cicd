#!/usr/bin/env python3
import socket

print(socket.gethostname())

def square(value):
    if isinstance(value,int):
        return value**2
    elif isinstance(value,str):
        return None


if __name__ == "__main__":
    print(square(3))
